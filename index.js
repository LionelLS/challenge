'use strict';

/**
 * ############################
 * ## Declaración de función ##
 * ############################
 */

// Llamamos a la función y le pasamos los argumentos. Guardamos
// lo que retorna este llamado en una cosntante.
const result = calculate('-', 4, 3);

// Mostramos el valor que ha retornado la función.
console.log(result);

// Declaración de función con tres parámetros.
function calculate (option, a = 0, b = 0) {
    if(option === '+') {
        // Retornamos la suma.
        return a + b;
    } else if (option === '-') {
        // Retornamos la resta.
        return a - b;
    } else {
        // Arrojamos un nuevo error. Esto hará que nuestro algoritmo
        // finalice por completo lanzando el mensaje de error.
        throw new Error('Tipo de operación incorrecto');
    }
} 

// Podemos comprobar lo que retorna la función metiendo el llamado
// dentro de un "console.log".
console.log(calculate('+', 5, 5));

/**
 * ##########################
 * ## Expresión de función ##
 * ##########################
 */

const sum = function (a, b) {
    return a + b;
}

console.log(sum(6, 6));

/**
 * #####################################
 * ## Función flecha o Arrow Function ##
 * #####################################
 */

// Con return implícito.
const sub_1 = (a, b) => a - b;

// Sin return implícito.
const sub_2 = (a, b) => {
    return a - b;
}

console.log(sub_1(10, 6))