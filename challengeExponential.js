//QUESTION Nº1

const approximateEx = () => {
    const xValue = Number(prompt(`Insert x value`));
    const nValue = Number(prompt(`Insert n value`));
    let myResult = 1; //Final result
    let divisor = 1;
    let dividend = xValue;

    for (let exponent = 1; exponent <= nValue; exponent++) {
        let agregattedDivisor = exponent * divisor; //variable which will accumulate the growing value of the divisor to eventually add it

        myResult += dividend / divisor; //we add the result of every division to the final result

        dividend = xValue * dividend; //we update the value of the variable

        divisor += agregattedDivisor; //we add the value for the next divisor
    }
    return myResult;
};

console.log(approximateEx()); //Finally, we can check the result in the browser's console

//QUESTION Nº2

//If n=24 then e≈2.663731258...
